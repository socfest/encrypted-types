<?php


namespace Socfest\ORM\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Socfest\Encrypter\OpenSSLEncrypter;

class OpenSSLEncoded extends StringType
{
    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return mixed|string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return OpenSSLEncrypter::encrypt($value);
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return array|mixed
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return OpenSSLEncrypter::decrypt($value);
    }

    /**
     * Gets the name of this type.
     *
     * @return string
     *
     * @todo Needed?
     */
    public function getName()
    {
        return 'encrypted.openssl';
    }
}
