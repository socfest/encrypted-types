<?php

namespace Socfest\ORM\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Socfest\Encrypter\Base64Encrypter;

class Base64Encoded extends StringType
{
    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return mixed|string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return Base64Encrypter::encrypt($value);
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return bool|mixed|string
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return Base64Encrypter::decrypt($value);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'encrypted.base64';
    }
}
