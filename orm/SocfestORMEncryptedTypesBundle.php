<?php

namespace Socfest\ORM;

use Doctrine\DBAL\Types\Type;
use Socfest\ORM\Types\Base64Encoded;
use Socfest\ORM\Types\OpenSSLEncoded;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\VarDumper\VarDumper;

class SocfestORMEncryptedTypesBundle extends Bundle
{
    /**
     * SocfestORMEncryptedTypesBundle constructor.
     * @throws \Doctrine\DBAL\DBALException
     */
    public function __construct()
    {
        Type::addType('encrypted_base64', Base64Encoded::class);
        Type::addType('encrypted_openssl', OpenSSLEncoded::class);
    }
}
