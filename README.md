Store database data encrypted

Encryption methods:
- base64
- openssl

# Install
Require package via composer
````
composer require socfest/encrypted-types
````

Init bundle in bundles.php
````
# config/bundles.php
return [
    // for ODM
    Socfest\MongoDB\SocfestMongoEncryptedTypesBundle::class => ['all' => true],
    
    //for ORM
    Socfest\ORM\SocfestORMEncryptedTypesBundle::class => ['all' => true],
]
````

You can specify the openssl cipher method by enter this to .env file. 

See the php.net *openssl_cipher_iv_length* page (https://www.php.net/manual/en/function.openssl-cipher-iv-length.php)

````dotenv
OPENSSL_CIPHER=%cipher_method%
````

# Usage

## ODM

In the model you can specify the field type. The new encrypted types are:
- encrypted_openssl
- encrypted_base64

````php
/**
 * @var string
 * @MongoDB\Field(type="encrypted_openssl")
 */
protected $fullName;
````

In query you can find by full value, and you have to encrypt before  set to equals.

Don't forget to use the correct encrypter!
````php
$this->dm->getRepository(User::class)
    ->createQueryBuilder()
    ->field('fullName')
    ->equals(OpenSSLEncrypter::encrypt('X Y'))
    ->getQuery()
    ->execure()
);
````
also work with find methods
````php
$this->dm->getRepository(User::class)
    ->findOneBy([
        'fullName' => OpenSSLEncrypter::encrypt('X Y')
    ])
````
