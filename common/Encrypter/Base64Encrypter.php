<?php


namespace Socfest\Encrypter;


class Base64Encrypter implements EncrypterInterface
{

    public static function encrypt($data)
    {
        return base64_encode(trim($data));
    }

    public static function decrypt($data)
    {
        return trim(base64_decode($data));
    }
}
