<?php


namespace Socfest\Encrypter;


use Symfony\Component\VarDumper\VarDumper;

class OpenSSLEncrypter implements EncrypterInterface
{
    /**
     * @param $data
     *
     * @return string
     */
    public static function encrypt($data)
    {
        $data = trim($data);
        $cipher= $_ENV["OPENSSL_CIPHER"]? $_ENV["OPENSSL_CIPHER"] : "AES256";
        $key = $_ENV['APP_SECRET'];
        $length = openssl_cipher_iv_length($cipher);
        $iv = substr($key, 0, $length);

        $encrypted = openssl_encrypt(
            $data,
            $cipher,
            $key,
            OPENSSL_RAW_DATA,
            $iv
        );

        return base64_encode($encrypted);
    }

    /**
     * @param $data
     *
     * @return array
     */
    public static function decrypt($data)
    {
        try {
            $data = base64_decode($data);
            $cipher= $_ENV["OPENSSL_CIPHER"]? $_ENV["OPENSSL_CIPHER"] : "AES256";
            $key = $_ENV['APP_SECRET'];
            $length = openssl_cipher_iv_length($cipher);
            $iv = substr($key, 0, $length);

            $decrypted = openssl_decrypt(
                $data,
                $cipher,
                $key,
                OPENSSL_RAW_DATA,
                $iv
            );
        } catch (\ErrorException $e) {
            @trigger_error($e->getMessage());
            $decrypted = $data;
        }

        return trim($decrypted);
    }
}
