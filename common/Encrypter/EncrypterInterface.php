<?php


namespace Socfest\Encrypter;


interface EncrypterInterface
{
    public static function encrypt($data);
    public static function decrypt($data);
}
