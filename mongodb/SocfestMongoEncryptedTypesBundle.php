<?php

namespace Socfest\MongoDB;

use Doctrine\ODM\MongoDB\Types\Type;
use Socfest\MongoDB\Types\Base64Encoded;
use Socfest\MongoDB\Types\OpenSSLEncoded;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SocfestMongoEncryptedTypesBundle extends Bundle
{
    public function __construct()
    {
        Type::registerType('encrypted_base64', Base64Encoded::class);
        Type::registerType('encrypted_openssl', OpenSSLEncoded::class);
    }
}
