<?php


namespace Socfest\MongoDB\Types;


use Doctrine\ODM\MongoDB\Types\Type;
use Socfest\Encrypter\Base64Encrypter;

class Base64Encoded extends Type
{
    public function convertToDatabaseValue($value)
    {
        return Base64Encrypter::encrypt($value);
    }

    public function convertToPHPValue($value)
    {
        return Base64Encrypter::decrypt($value);
    }

    public function closureToPHP() : string
    {
        return '$return = $value === null ? null : \Socfest\Encrypter\Base64Encrypter::decrypt($value);';
    }
}
