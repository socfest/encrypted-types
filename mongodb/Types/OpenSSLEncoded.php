<?php


namespace Socfest\MongoDB\Types;

use Doctrine\ODM\MongoDB\Types\Type;
use Socfest\Encrypter\OpenSSLEncrypter;

class OpenSSLEncoded extends Type
{
    public function convertToDatabaseValue($value)
    {
        return OpenSSLEncrypter::encrypt($value);
    }

    public function convertToPHPValue($value)
    {
        return OpenSSLEncrypter::decrypt($value);
    }

    public function closureToMongo() : string
    {
        return '$return = $value === null ? null : \Socfest\Encrypter\OpenSSLEncrypter::encrypt($value);';
    }

    public function closureToPHP() : string
    {
        return '$return = $value === null ? null : \Socfest\Encrypter\OpenSSLEncrypter::decrypt($value);';
    }
}
