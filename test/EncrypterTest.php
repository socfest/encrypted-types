<?php

namespace Socfest\Tests;

use PHPUnit\Framework\TestCase;
use Socfest\Encrypter\OpenSSLEncrypter;

class EncrypterTest extends TestCase
{
    public function testOpenSSL() {
        putenv('APP_SECRET='.random_bytes(32));

        $text = 'Magadba szívni, amit csak lehet';

        $enctypted = OpenSSLEncrypter::encrypt($text);
        $decrypted = OpenSSLEncrypter::decrypt($enctypted);

        $this->assertEquals(
            $decrypted,
            $text
        );
    }
}
